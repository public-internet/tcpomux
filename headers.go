package tcpmux
import "bufio"
import "errors"
import "fmt"
import "io"
import "net/textproto"
import "os"

type request struct {
	Method string
	Option string
	Header map[ string ]string
}

func ( self request )Write( stream io.ReadWriter ) error {
	var err error

	_ , err = fmt.Fprintf( stream , "%s %s KONN/0.3\r\n" , self.Method , self.Option )
	if err != nil {
		return err }

	for _ , item := range self.Header {
		_ , err = fmt.Fprintf( stream , "%s: %s\r\n" , item , self.Header[ item ] )
		if err != nil {
			return err }
	}

	_ , err = fmt.Fprintf( stream , "\r\n" )
	if err != nil {
		return err }

	return nil
}

func ( self * request )Parse( bufread * bufio.Reader , stream io.Writer ) error {
	var err error
	var reader textproto.Reader = textproto.Reader{
		R : bufread ,
	}

	buffer , err := reader.ReadLine( )
	if err != nil {
		return err }
	if  len( buffer ) < 10 ||
	    buffer[ len( buffer ) - 9 : ] != " KONN/0.3" {
		_ , _ = fmt.Fprintf( stream , "KONN/0.3 protocol mismatch\r\n\r\n" )
		return errors.New( "protocol mismatch" ) }
	count , err := fmt.Sscanf( buffer , "%16s %512s KONN/0.3\r\n" , & self.Method , & self.Option )
	if err != nil {
		_ , _ = fmt.Fprintf( stream , "KONN/0.3 %v\r\n\r\n" , err )
		return err }
	if count != 2 {
		_ , _ = fmt.Fprintf( stream , "KONN/0.3 unknown request\r\n\r\n" )
		return errors.New( "unknown request" ) }

	headers , err := reader.ReadMIMEHeader( )
	if err != nil {
		_ , _ = fmt.Fprintf( stream , "KONN/0.3 %v\r\n\r\n" , err )
		return err }
	for item , value := range headers {
		if len( value ) == 1 {
			self.Header[ item ] = value[ 0 ]
		} else {
			for _ , value := range value {
				self.Header[ item ] = self.Header[ item ] + "; " + value
			}
		}
	}

	fmt.Fprintf( os.Stderr , "D Left over bytes %v\r\n" , reader.R.Buffered( ) )
	return nil
}

type response struct {
	Status string
	Header map[ string ]string
}

func ( self response )Write( stream io.ReadWriter ) error {
	var err error
	if self.Status == "" {
		self.Status = "OKAY" }

	_ , err = fmt.Fprintf( stream , "KONN/0.3 %s\r\n" , self.Status )
	if err != nil {
		return err }

	for key , value := range self.Header {
		_ , err = fmt.Fprintf( stream , "%s: %s\r\n" , key , value )
		if err != nil {
			return err }
	}

	_ , err = fmt.Fprintf( stream , "\r\n" )
	if err != nil {
		return err }

	return nil
}

func ( self * response )Parse( bufread * bufio.Reader , stream io.Writer ) error {
	var err error
	var reader textproto.Reader
	reader.R = bufread

	buffer , err := reader.ReadLine( )
	if err != nil {
		return err }
	if  len( buffer ) < 10 ||
	    buffer[ 0 : 9 ] != "KONN/0.3 " {
		return errors.New( "protocol mismatch" ) }
	self.Status = buffer[ 9 : ]
	if self.Status != "OKAY" {
		return errors.New( self.Status ) }

	headers , err := reader.ReadMIMEHeader( )
	if err != nil {
		return err }
	for item , value := range headers {
		if len( value ) == 1 {
			self.Header[ item ] = value[ 0 ]
		} else {
			for _ , value := range value {
				self.Header[ item ] = self.Header[ item ] + "; " + value
			}
		}
	}

	return nil
}


