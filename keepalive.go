package tcpmux
import "io"
import "time"

// keepalive
// -	Monitors stream
// -	Makes sure there's at least one stream.Read( ) every timeout
// -	Calls pokeit( ) if no stream.Read( ) after timeout
// -	Calls expire( ) if still no stream.Read( ) after timeout
type keepalive struct{
	self * keepalive	// :(
	stream io.ReadWriteCloser
	timeout time.Duration
	pokeit func( )
	expire func( )
	count uint64
	closed bool
}

// fun fact: go net.Conn.Write( ) does not wait for TCP ACKs
func ( self keepalive )Write( buffer [ ]byte )( int , error ) {
	length , err := self.stream.Write( buffer )
	if err != nil {
		self.self.closed = true }
	return length , err
}

func ( self keepalive )Read( buffer[ ]byte )( int , error ) {
	self.self.count += 1
	return self.stream.Read( buffer )
}

func ( self keepalive )Close( )error {
	self.self.closed = true
	return self.self.Close( )
}

// 
func ( self * keepalive )wait( ) {
	var count uint64
	for self.closed == false {
		count = self.count
		time.Sleep( self.timeout / 2 )
		if self.count == count {
			self.pokeit( )
			time.Sleep( self.timeout )
			if self.count == count {
				self.expire( )
				return
			}
		}
	}
}

// Init
func newKeepalive( stream io.ReadWriteCloser , timeout time.Duration , pokeit func( ) , expire func( ) )keepalive {
	var keepa keepalive = keepalive{
		stream : stream ,
		timeout : timeout ,
		pokeit : pokeit ,
		expire : expire ,
	}
	keepa.self = & keepa
	if timeout != 0 {
		go keepa.wait( )
	}
	return keepa
}

