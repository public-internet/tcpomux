package tcpmux
import "io"
import "net"
import "testing"
import "time"

func TestKeepalive( test * testing.T ) {
	var err error
	var keepa keepalive
	var pokeit , expire int
	var strone , strtwo io.ReadWriteCloser
	strone , strtwo = net.Pipe( )

	go func( ){
		var buffer [ ]byte = [ ]byte{ 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , }
		for {
			_ , err = strtwo.Read( buffer )
			_ , err = strtwo.Write( buffer )
		}
	}( )

	keepa = newKeepalive( strone , 1 * time.Second , func( ){
		pokeit ++
	} , func( ){
		expire ++
	} )

	var buffer [ ]byte = [ ]byte{ 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , }
	_ , err = keepa.Write( [ ]byte( "Hello!" ) )
	if err != nil {
		test.Fatalf( "%v\r\n" , err ) }
	_ , err = keepa.Read( buffer )
	if err != nil {
		test.Fatalf( "%v\r\n" , err ) }
	if pokeit != 0 {
		test.Fatalf( "pokeit %v\r\n" , pokeit ) }

	time.Sleep( 1100 * time.Millisecond )
	if pokeit != 1 {
		test.Fatalf( "pokeit %v\r\n" , pokeit ) }
	_ , err = keepa.Write( [ ]byte( "Hello!" ) )
	if err != nil {
		test.Fatalf( "%v\r\n" , err ) }
	_ , err = keepa.Read( buffer )
	if err != nil {
		test.Fatalf( "%v\r\n" , err ) }	

	if expire != 0 {
		test.Fatalf( "expire %v\r\n" , expire ) }
	time.Sleep( 2600 * time.Millisecond )	// ?
	if pokeit != 2 {
		test.Fatalf( "pokeit %v\r\n" , pokeit ) }
	if expire != 1 {
		test.Fatalf( "expire %v\r\n" , expire ) }

	return

}

