module gitlab.com/public-internet/tcpomux

go 1.15

require (
	github.com/KonaArctic/Token v0.0.0-20211221234727-ba4a8769400a // indirect
	github.com/KonaArctic/token v0.0.0-20211221040915-d1f70e2610cb // indirect
	github.com/libp2p/go-mplex v0.3.0
)
