package tcpmux
import "net"
import "testing"

func TestNew( test * testing.T ) {
	var err error
	var confone , conftwo Config
	var socone , soctwo , socket net.Conn
	var buffer [ ]byte = [ ]byte{ 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 }
	socone , soctwo = net.Pipe( )

	// Start connection
	conftwo.Dialer = net.Dial
	go func( ){
		err = conftwo.Start( soctwo )
		if err != nil {
			test.Fatalf( "%v\r\n" , err ) }
	}( )
	err = confone.Start( socone )
	if err != nil {
		test.Fatalf( "%v\r\n" , err ) }
	defer confone.End( )

	// Test error handleing
	_ , err = confone.Dialer( "tcp" , "notahost:23" )
	if  err == nil ||
	    err.Error( )[ 0 : 29] != "dial tcp: lookup notahost on " {
		test.Fatalf( "not error" )
	 }

	// Test connection making
	socket , err = confone.Dialer( "tcp" , "example.com:80" )
	if err != nil {
		test.Fatalf( "%v\r\n" , err ) }
	_ , err = socket.Write( [ ]byte( "GET / HTTP/1.1\r\nHost: example.com\r\n\r\n" ) )
	if err != nil {
		test.Fatalf( "%v\r\n" , err ) }
	_ , err = socket.Read( buffer )
	if err != nil {
		test.Fatalf( "%v\r\n" , err ) }
	if string( buffer ) != "HTTP/1.1" {
		test.Fatalf( "Output mismatch: %v\r\n" , string( buffer ) ) }

	// Test LowLat
	// FIXME: Test needs simulated latency
	confone.LowLat = true
	socket , err = confone.Dialer( "tcp" , "notahost:23" )
	_ , err = socket.Read( [ ]byte( buffer ) )
	if  err == nil ||
	    err.Error( )[ 0 : 29] != "dial tcp: lookup notahost on " {
		test.Fatalf( "not error %v" , buffer )
	 }

	socket , err = confone.Dialer( "tcp" , "example.com:80" )
	_ , err = socket.Write( [ ]byte( "GET / HTTP/1.1\r\nHost: example.com\r\n\r\n" ) )
	if err != nil {
		test.Fatalf( "%v\r\n" , err ) }
	_ , err = socket.Read( buffer )
	if err != nil {
		test.Fatalf( "%v\r\n" , err ) }
	if string( buffer ) != "HTTP/1.1" {
		test.Fatalf( "Output mismatch: %v\r\n" , string( buffer ) ) }

	return
}

