// Convert io.ReadWriteCloser to fake net.Conn
package tcpmux
import "bufio"
import "io"
import "net"
import "time"

// Fake net.Addr
type fakeaddr struct{ }

func ( self fakeaddr )Network( )string {
	return "fakeaddr"
}

func ( self fakeaddr )String( )string {
	return "fakeaddr"
}

// Fake net.Conn
type fakeconn struct{
	local net.Addr
	remote net.Addr
	reader * bufio.Reader
	stream io.ReadWriteCloser
	errblock * chan error
}

func ( self fakeconn )LocalAddr( )net.Addr {
	var fakeaddr fakeaddr
	return fakeaddr
}

func ( self fakeconn )RemoteAddr( ) net.Addr{
	var fakeaddr fakeaddr
	return fakeaddr
}

func ( self fakeconn )SetDeadline( time time.Time )error {
	return nil
}

func ( self fakeconn )SetReadDeadline( time time.Time )error {
	return nil
}

func ( self fakeconn )SetWriteDeadline( time time.Time )error {
	return nil
}

func ( self fakeconn )Read( buffer [ ]byte )( int , error ) {
	if self.errblock != nil &&
	   * self.errblock != nil {
		err := <- * self.errblock
		* self.errblock = nil
		if err != nil {
			_ = self.stream.Close( )
			return 0 , err }
	}
	return self.reader.Read( buffer )
}

func ( self fakeconn )Write( buffer [ ]byte )( int , error ) {
	if  self.errblock != nil &&
	    len( * self.errblock ) > 0 {
		err := <- * self.errblock
		* self.errblock = nil
		if err != nil {
			_ = self.stream.Close( )
			return 0 , err }
	}
	return self.stream.Write( buffer )
}

func ( self fakeconn )Close( )error {
	if self.errblock != nil {
		self.errblock = nil
	}
	return self.stream.Close( )
}

// Wrapper
func wrapconn( stream io.ReadWriteCloser )net.Conn {
	var fakeaddr fakeaddr
	var errblock chan error = make( chan error , 4 )
	return fakeconn{
		stream : stream ,
		local : fakeaddr ,
		remote : fakeaddr ,
		errblock : & errblock ,
	}
}

