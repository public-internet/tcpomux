package tcpmux
import "bufio"
import "context"
import "errors"
import "github.com/libp2p/go-mplex"
import "io"
import "net"
import "github.com/KonaArctic/Token"
import "time"

type Dialer func ( network string , address string ) ( net.Conn , error )

type Config struct{
	Dialer Dialer
	Header map[ string ]string
	Keepalive func( )time.Duration	// :(
	Token any
	LowLat bool
	stream io.ReadWriteCloser
	mydial Dialer
}

// 
func ( self * Config )Start( stream io.ReadWriteCloser )error {
	var err error
	var ok bool
	var mplex multiplex.Multiplex
	var context2 context.Context = context.Background( )
	var request2 request
//	var response2 request
	var reader * bufio.Reader = bufio.NewReader( stream )
	self.stream = stream

	// Handshake
	_ , ok = self.Token.( string )
	if ! ok {
		_ , ok = self.Token.( token.Token )
		if ok {
			self.Token = self.Token.( token.Token ).String( )
		} else {
			self.Token = "!anon"
		}
	}
	var errpipe chan error = make( chan error )
	go func( ){
		errpipe <- request{
			Method : "START" ,
			Option : self.Token.( string ) ,
		}.Write( stream )
	}( )
	err = request2.Parse( reader , stream )
	if <- errpipe != nil {
		return <- errpipe }
	if err != nil {
		return err }
	if request2.Method != "START" {
		response{
			Status : "unknown request" ,
		}.Write( stream )
		return errors.New( "unknown request" )
	}
	if request2.Option == "!anon" {
		self.Token = nil
	} else {
		self.Token , err = token.Parse( request2.Option )
		if err != nil {
			response{
				Status : err.Error( ) ,
			}.Write( stream )
			return err
		}
	}
	self.Header = request2.Header

//	FIXME
//	go func( ){
//		errpipe <- response{ }.Write( stream )
//	}( )
//	err = response2.Parse( stream )
//	if <- errpipe != nil {
//		return <- errpipe }
//	if err != nil {
//		return dialer , err }

	// Timeouts
//	if self.Keepalive != nil {
//		timeout := self.Keepalive( )
//		context2 , _ = context.WithTimeout( context2 , timeout )
//		stream = newKeepalive( stream , timeout , func( ){
//			_ , _ = mplex.NewStream( context2 )
//		} , func( ){
//			self.Keepalive( )
//		} )
//		self.stream = stream
//	}

	// Create multiplex
	socone , soctwo := net.Pipe( )	// work-around
	go io.Copy( soctwo , reader )
	go io.Copy( stream , soctwo )
	mplex = * multiplex.NewMultiplex( socone , false )

	// Process reciving
	go func( dialer Dialer ) {
		for err == nil {
			var stream io.ReadWriteCloser
			var reader * bufio.Reader
			stream , err = mplex.Accept( )
			reader = bufio.NewReader( stream )
			go func( ) {

				if dialer == nil {
					_ = response{
						Status : "self.Dialer is not defined" ,
					}.Write( stream )
					return }

				// Get request
				err = request2.Parse( reader , stream )
				if err != nil {
					_ = response{
						Status : err.Error( ) ,
					}.Write( stream )
					return }

				// Make connection
				if request2.Method != "TCP" {
					_ = response{
						Status : "protocol not implemented" ,
					}.Write( stream )
					return }

				socket , err := dialer( "tcp" , request2.Option )
				if err != nil {
					_ = response{
						Status : err.Error( ) ,
					}.Write( stream )
					return }

				err = response{ }.Write( stream )
				if err != nil {
					_ = response{
						Status : err.Error( ) ,
					}.Write( stream )
					return }

				// Shuttle data
				go io.Copy( stream , socket )
				go io.Copy( socket , reader )

			}( )
		}
	}( self.Dialer )

	// Dialer
	self.mydial = self.Dialer
	self.Dialer = func ( network string , address string ) ( net.Conn , error ) {
		var err error
		var stream fakeconn = fakeconn{ }
		var response response
		if network != "tcp" {
			return stream , errors.New( "protocol not implemented" ) }

		// Open stream
		stream.stream , err = mplex.NewStream( context2 )
		stream.reader = bufio.NewReader( stream.stream )
		if err != nil {
			stream.Close( )
			return stream , err }

		// Ask for connection
		_ = request{
			Method : "TCP" ,
			Option : address ,
		}.Write( stream )

		// Parse response
		if self.LowLat == false {
			err = response.Parse( stream.reader , stream.stream )
			if err != nil {
				stream.Close( )
				return stream , err }

		// Return now and report errors later. Most connections will probably be successful; returning now allows the caller to write to this connection immediately (e.g. sending HTTP request) thus possibly reduce latency.
		} else {
			var errblock chan error = make( chan error , 1 )
			stream.errblock = & errblock
			go func( ){
				err = response.Parse( stream.reader , stream.stream )
				* stream.errblock <- err
			}( )
	
		}

		// Done
		return stream , nil

	}
	
	return nil
}


func ( self * Config )End( )error {
	self.Dialer = self.mydial
	if self.stream != nil {
		return self.stream.Close( )
	}
	return nil
}

